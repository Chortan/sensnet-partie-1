#include "DHTesp.h"

uint8_t DHTpin = D5;
DHTesp dht;

void setup() {
  Serial.begin(9600);
  dht.setup(DHTpin, DHTesp::DHT22);
}


void loop() {
  TempAndHumidity measurement = measure();
  Serial.println(jsonify(measure()));
  delay(1000);
}

TempAndHumidity measure() {
  return dht.getTempAndHumidity();
}

String jsonify(TempAndHumidity measure) {
  String temperature;
  String humidity;
  
  if(isnan(measure.temperature)) {
    temperature = "null";
  } else {
    temperature = String(measure.temperature); 
  }

  if(isnan(measure.humidity)) {
    humidity = "null";
  } else {
    humidity = String(measure.humidity); 
  }
  
  return "{\"temperature\": " + temperature + ", \"humidity\": " + humidity + "}" ;
}
